<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.*" %>

<c:set var="wiki_lang"><wiki:Variable var='jspwiki.Language' /></c:set>
<c:set var="wiki_project_name"><wiki:Variable var='jspwiki.projname' /></c:set>

<fmt:setLocale value="${wiki_lang}"/>
<fmt:setBundle basename="messages"/>

<div class="row">
	<div class="col-xs-3">
		<img id="puma-logo-img" src="<wiki:Variable var='jspwiki.resources.prefix'/>/resources_puma/image/puma_logo.png"/>
	</div>
	<div class="col-xs-9">
		<h1>
			<a title="home" href="/"><c:out value="${wiki_project_name}"/></a>
		</h1>
		<p class="logo-desc"><small><fmt:message key="slogan"/></small></p>
	</div>
</div>