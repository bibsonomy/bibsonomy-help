#! /bin/bash
# Deploy the german and the english help to a server
# specify the system (bibsonomy or puma) and a profile (see pom.xml of the according project) to deploy
# e.g. ./deploy puma ffm_rc
# Example ./deploy.sh bibsonomy gromit 'ru en de'

project=$1
targetProfile=$2
languages=$3 # to specify more than one languages use 'lang1 lang2'

# set en de and ru as the default languages when no specific language(s) is/are specified
if [ -z ${languages} ]; then
  languages="en de ru";
fi

for lang in ${languages}; do
  echo "---------------------------------------------------"
  echo "Deploying help in language $lang to $targetProfile"
  echo "---------------------------------------------------"

  mvn tomcat7:deploy -Dhelp-language=$lang -Dtomcat-server=$targetProfile -f bibsonomy-help-$project/pom.xml
  if [ $? -ne 0 ]; then
   exit;
  fi;
done
