# DEPRECATION NOTE

This project was a modified JSPwiki for the BibSonomy help. We replaced the JSPWiki with a file based markdown system in 2017.

# README #

This is the webapp of a BibSonomy/PUMA help. The webapp is parametrized using the jspwiki_xx.property files where xx is the ISO 639 short for the language.


##Release 1.0.0##
* introduced new variables to handle more than one language (until now we had only one other language and one variable simply for "the other" language. Now we need individual variables for each language


```
#!bash

#DEPRECATED - was used to point to "the other" language
jspwiki.interWikiRef.Bib_Help = http://www.bibsonomy.org/help_en

jspwiki.interWikiRef.BibHelpRu = http://www.bibsonomy.org/help_ru
jspwiki.interWikiRef.BibHelpDe = http://www.bibsonomy.org/help_de
jspwiki.interWikiRef.BibHelpEn = http://www.bibsonomy.org/help_en

```