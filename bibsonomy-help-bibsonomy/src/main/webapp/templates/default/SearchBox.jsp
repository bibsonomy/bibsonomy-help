<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.*" %>

<c:set var="wiki_lang"><wiki:Variable var='jspwiki.Language' /></c:set>

<fmt:setLocale value="${wiki_lang}"/>
<fmt:setBundle basename="messages"/>
<%-- Provides a simple searchbox that can be easily included anywhere on the page --%>
<%-- Powered by jswpwiki-common.js//SearchBox --%>

<fmt:message var="search" key="search"/>
	
<div id="search">

	<form action="<wiki:Link jsp='Search.jsp' format='url'/>" class="smallform" accept-charset="<wiki:ContentEncoding />">
	
		<div class="input-group">
			<div class="input-group-addon">
				<span class="search-scope"><fmt:message key="search"/></span>
			</div>

  			<input onblur="if( this.value == '' ) { this.value = this.defaultValue; this.className='descriptiveLabel form-control' }; return true; "
				onfocus="if( this.value == this.defaultValue ) { this.value = ''; this.className='form-control' }; return true; "
				   type="text" value="${search}"
				   name="query" id="inpf"
				   size="20" 
				   class="descriptiveLabel form-control"
			      accesskey="f"
			      />
  			
  			
  			<span class="input-group-btn">
  				<button class="btn btn-default" type="submit"
  				
  				name="searchSubmit"
  				value="${search}"
			  	title="${search}"
			  		
  				><span class="fa fa-search"><!--  --></span></button>
  		</div>
	</form>

	
</div>