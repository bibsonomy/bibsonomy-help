<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.attachment.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.*" %>

<c:set var="wiki_lang"><wiki:Variable var='jspwiki.Language' /></c:set>

<fmt:setLocale value="${wiki_lang}"/>
<fmt:setBundle basename="messages"/>
<%
  WikiContext c = WikiContext.findContext( pageContext );
  int attCount = c.getEngine().getAttachmentManager().listAttachments(c.getPage()).size();
  String attTitle = LocaleSupport.getLocalizedMessage(pageContext, "attach.tab");
  if( attCount != 0 ) attTitle += " (" + attCount + ")";
%>
<c:if test="${!empty param.oldpage}">
	<div class="information">
	     <div align="center">
		<h3>
			<fmt:message key="non.existing.page">
				<fmt:param>
					<c:out value="${param.oldpage}"/>
				</fmt:param>
			</fmt:message>
		</h3>
	     </div>
	  <form action="<wiki:Link jsp='Search.jsp' format='url'/>" class="wikiform" id="searchForm" accept-charset="<wiki:ContentEncoding />">
		<div align="center">
	  
		  <input   type="text" value="<c:out value="${param.oldpage}"/>"
			   name="query" id="query"
			   size="20" 
	      		   accesskey="f" />
		

		  <button type="submit" style="height:30px; width:100px; align:center"
		  		 name="searchSubmit" 
		  		value="Search"
		  		title="Search">
			<fmt:message key="search"/>
		  </button>
		</div>
	</form>
  </div>
</c:if>

<wiki:UserCheck status="notAuthenticated">
  <div id="pagecontent">
	<wiki:Include page="PageTab.jsp"/>
    <wiki:PageType type="attachment">
      <div class="information">
	    <fmt:message key="info.backtoparentpage" >
	      <fmt:param><wiki:LinkToParent><wiki:ParentPageName/></wiki:LinkToParent></fmt:param>
        </fmt:message>
      </div>
      <div style="overflow:hidden;">
        <wiki:Translate>[<%= c.getPage().getName()%>]</wiki:Translate>
      </div>
    </wiki:PageType>    
  </div>
</wiki:UserCheck>

<wiki:UserCheck status="authenticated">

	<ul class="nav nav-tabs">
		<li  class="active">
			<a id="pagecontentTab" data-toggle="tab" href="#pagecontent">
				<fmt:message key="view.tab"/>
			</a>
		</li>
		<li>
			<a id="attachTab" data-toggle="tab" href="#attach">
				<fmt:message key="attach.tab"/>
			</a>
		</li>
		<li>
			<a id="infoTab" href="<%=c.getURL(WikiContext.INFO, c.getPage().getName())%>">
				<fmt:message key="info.tab"/>
			</a>
		</li>
	</ul>

	<div class="tab-content">

		<div id="pagecontent" class="tab-pane active">
			<wiki:Include page="PageTab.jsp"/>
    		<wiki:PageType type="attachment">
      			<div class="information">
	    			<fmt:message key="info.backtoparentpage" >
	  	    			<fmt:param><wiki:LinkToParent><wiki:ParentPageName/></wiki:LinkToParent></fmt:param>
        			</fmt:message>
      			</div>
      			<div style="overflow:hidden;">
        			<wiki:Translate>[<%= c.getPage().getName()%>]</wiki:Translate>
      			</div>
    		</wiki:PageType>    
		</div>

  		<wiki:PageExists>
  			<wiki:PageType type="page">
  				<div id="attach" class="tab-pane">
    				<wiki:Include page="AttachmentTab.jsp"/>
  				</div>
  			</wiki:PageType>
    
  			<wiki:Tab id="info" title='<%=LocaleSupport.getLocalizedMessage(pageContext, "info.tab")%>'
           		url="<%=c.getURL(WikiContext.INFO, c.getPage().getName())%>"
           		accesskey="i" >
  			</wiki:Tab>
  		</wiki:PageExists>
	</div>
</wiki:UserCheck>
