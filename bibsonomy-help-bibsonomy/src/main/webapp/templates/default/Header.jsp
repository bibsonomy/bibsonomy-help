<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.ecyrd.jspwiki.*" %>

<c:set var="wiki_lang"><wiki:Variable var='jspwiki.Language' /></c:set>
<c:set var="wiki_project_name"><wiki:Variable var='jspwiki.projname' /></c:set>


<fmt:setLocale value="${wiki_lang}"/>
<fmt:setBundle basename="messages"/>
<%
  WikiContext c = WikiContext.findContext(pageContext);
  String frontpage = c.getEngine().getFrontPage(); 
%>


<div class="container header">

	<div id="page-header" class="bib-header">
	
		<div class="row">
	
		<div class="col-md-6">
			<!-- Logo include -->
			<%-- include file="logo.jsp" --%>
			<wiki:Include page="logo.jsp" />
		</div>
		
		<div class="col-md-6">
			
			<div id="lang-switcher" class="pull-right">
				<c:set var="lang_en_page"><wiki:Variable var='lang_en' /></c:set>
				<c:if test="${'No such variable: No variable lang_en defined.' eq  lang_en_page}">
				   	 <c:set var="lang_en_page" value="Main"/>
				</c:if>
 				<c:set var="lang_de_page"><wiki:Variable var='lang_de' /></c:set>
				<c:if test="${'No such variable: No variable lang_de defined.' eq  lang_de_page}">
				   	 <c:set var="lang_de_page" value="Main"/>
				</c:if>
 				<c:set var="lang_ru_page"><wiki:Variable var='lang_ru' /></c:set>
				<c:if test="${'No such variable: No variable lang_ru defined.' eq  lang_ru_page}">
				   	 <c:set var="lang_ru_page" value="Main"/>
				</c:if>
 
				<c:choose>
					<c:when test="${wiki_lang eq 'de'}">
					
						<span>(</span>
						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpEn' />${lang_en_page}">
							<fmt:message key="en"/>
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="#">
							<fmt:message key="de"/>
						</a>
						<span>&nbsp;|&nbsp;</span>
<!-- 						Link to the main page while we do not have a full Russian help wiki -->
<%-- 						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpRu' />/<wiki:Variable var='lang_ru' />"> --%>
						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpRu' />${lang_ru_page}">
							<fmt:message key="ru"/>
						</a>
						<span>)</span>
						
					</c:when>
					<c:when test="${wiki_lang eq 'ru'}">
					
						<span>(</span>
						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpEn' />${lang_en_page}">
							<fmt:message key="en"/>
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpDe' />${lang_de_page}">
							<fmt:message key="de"/>
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="#">
							<fmt:message key="ru"/>
						</a>
						<span>)</span>
						
					</c:when>
					<c:otherwise>
					
						<span>(</span>
						<a href="#">
							<fmt:message key="en"/>
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpDe' />${lang_de_page}">
							<fmt:message key="de"/>
						</a>
						<span>&nbsp;|&nbsp;</span>
<!-- 						Link to the main page while we do not have a full Russian help wiki -->
<%-- 						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpRu' />/<wiki:Variable var='lang_ru' />"> --%>
						<a href="<wiki:Variable var='jspwiki.interWikiRef.BibHelpRu' />${lang_ru_page}">
							<fmt:message key="ru"/>
						</a>
						<span>)</span>
						
					</c:otherwise>
				</c:choose>
				
			
			</div>
			
			 <br style="clear: both;" />
			 <p style="line-height: 9px; font-size:9px; padding:0;margin:0;">&nbsp;<!-- KEEP ME --></p>
			 
			 <!-- Search -->
			 <wiki:Include page="SearchBox.jsp" />
			
		</div>
	</div>
	
	</div>

</div>


<wiki:Include page="Navigation.jsp" />


<div id="header">
	<div id="poststats"></div>
</div>
