<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.*" %>

<c:set var="wiki_lang"><wiki:Variable var='jspwiki.Language' /></c:set>
<c:set var="wiki_project_name"><wiki:Variable var='jspwiki.projname' /></c:set>

<fmt:setLocale value="${wiki_lang}"/>
<fmt:setBundle basename="messages"/>
<!-- Bibsonomy -->
<div id="logo">
	<h1>
		<a href="/" title="<fmt:message key="home"/>">
			<c:out value="${wiki_project_name}"/>
		</a>
	</h1>
	<p style="color:#006699;"><small><fmt:message key="slogan"/></small></p>
</div>
