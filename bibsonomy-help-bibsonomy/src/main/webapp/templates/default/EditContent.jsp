<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.*" %>
<fmt:setLocale value="${prefs.Language}" />
<fmt:setBundle basename="templates.default"/>
<%
  WikiContext c = WikiContext.findContext( pageContext );
  int attCount = c.getEngine().getAttachmentManager().listAttachments(c.getPage()).size();
  String attTitle = LocaleSupport.getLocalizedMessage(pageContext, "attach.tab");
  if( attCount != 0 ) attTitle += " (" + attCount + ")";
%>
  
<ul class="nav nav-tabs">
	<li  class="active">
		<a id="editcontentTab" data-toggle="tab" href="#editcontent">
			<%=LocaleSupport.getLocalizedMessage(pageContext,"edit.tab.edit")%>
		</a>
	</li>
	<li>
		<a id="attachTab" data-toggle="tab" href="#attach">
			<fmt:message key="attach.tab"/>
		</a>
	</li>
	<li>
		<a id="infoTab" href="<%=c.getURL(WikiContext.INFO, c.getPage().getName())%>">
			<fmt:message key="info.tab"/>
		</a>
	</li>
</ul>

<div class="tab-content">
  
	<div id="editcontent"  class="tab-pane active">
  		<wiki:CheckLock mode="locked" id="lock">
    		<div class="error">
      			<fmt:message key="edit.locked">
        			<fmt:param><c:out value="${lock.locker}"/></fmt:param>
        			<fmt:param><c:out value="${lock.timeLeft}"/></fmt:param>
      			</fmt:message>
    		</div>
  		</wiki:CheckLock>
  
		<wiki:CheckVersion mode="notlatest">
    		<div class="warning">
      			<fmt:message key="edit.restoring">
        			<fmt:param><wiki:PageVersion/></fmt:param>
	      		</fmt:message>
    		</div>
  		</wiki:CheckVersion>
    
		<wiki:Editor />
    
	</div>
  
	<wiki:PageExists>  

		<div id="attach" class="tab-pane">
  	  		<wiki:Include page="AttachmentTab.jsp"/>
  		</div>

  	</wiki:PageExists>  
    
	<div id="edithelp"  class="tab-pane">
		<wiki:InsertPage page="EditPageHelp" />
  		<wiki:NoSuchPage page="EditPageHelp">
		    <div class="error">
    			<fmt:message key="comment.edithelpmissing">
        			<fmt:param><wiki:EditLink page="EditPageHelp">EditPageHelp</wiki:EditLink></fmt:param>
      			</fmt:message>
    		</div>
  		</wiki:NoSuchPage>  
  	</div>
</div>