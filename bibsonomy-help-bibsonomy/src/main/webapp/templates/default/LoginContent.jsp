<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki"%>
<%@ page import="com.ecyrd.jspwiki.*"%>
<%@ page import="com.ecyrd.jspwiki.*"%>
<%@ page import="com.ecyrd.jspwiki.auth.*"%>
<%@ page errorPage="/Error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.servlet.jsp.jstl.fmt.*"%>

<%--<fmt:setLocale value="${prefs.Language}" /> --%>
<fmt:setBundle basename="templates.default" />

<c:set var="oFLogin">
	<wiki:Variable var='jspwiki.openForLogin' />
</c:set>
<fmt:setLocale value="${oFLogin}" />


<c:set var="wiki_lang">
	<wiki:Variable var='jspwiki.Language' />
</c:set>
<c:set var="helpLangUrl">
	<wiki:Variable var='jspwiki.baseURL' />
</c:set>

<fmt:setLocale value="${wiki_lang}" />


<%
    String postURL = "";
    WikiContext ctx = WikiContext.findContext( pageContext );
    AuthenticationManager mgr = ctx.getEngine().getAuthenticationManager();

    if( mgr.isContainerAuthenticated() )
    {
        postURL = "j_security_check";
    }
    else
    {
        String redir = (String)ctx.getVariable("redirect");
        if( redir == null ) redir = ctx.getEngine().getFrontPage();
        postURL = ctx.getURL( WikiContext.LOGIN, redir );
    }

    boolean supportsCookieAuthentication = mgr.allowsCookieAuthentication();
%>

<%-- Hide the login if login is disabled --%>
<c:choose>
	<c:when test="${oFLogin == 'false'}">


		<h3>
			<fmt:message key="login.heading.login">
				<fmt:param>
					<wiki:Variable var="applicationname" />
				</fmt:param>
			</fmt:message>
		</h3>

		<fmt:setBundle basename="messages" />
		<p>
			<fmt:message key="login.disabled" />
		</p>
		<p>
			<input type="button" value="<fmt:message key="login.back"/>"
				onclick="window.history.back()" />
		</p>

	</c:when>

	<c:otherwise>

		<ul class="nav nav-tabs">
			<li  class="active">
				<a id="logincontentTab" data-toggle="tab" href="#logincontent">
					<fmt:message key="login.tab"/>
				</a>
			</li>
			<li>
				<a id="lostpasswordTab" data-toggle="tab" href="#lostpassword">
					<fmt:message key="login.lostpw.tab"/>
				</a>
			</li>
			<li>
				<a id="profileTab" data-toggle="tab" href="#profile">
					<fmt:message key="login.register.tab"/>
				</a>
			</li>
			<li>
				<a id="loginhelpTab" data-toggle="tab" href="#loginhelp">
					<fmt:message key="login.tab.help"/>
				</a>
			</li>
		</ul>

		<div class="tab-content">


		<wiki:UserCheck status="notauthenticated">

			<div id="logincontent" class="tab-pane active">

				<form action="<%=postURL%>" id="login" class="wikiform"
					onsubmit="return Wiki.submitOnce(this);" method="post"
					accept-charset="<wiki:ContentEncoding />">

					<div class="center">

						<h3>
							<fmt:message key="login.heading.login">
								<fmt:param>
									<wiki:Variable var="applicationname" />
								</fmt:param>
							</fmt:message>
						</h3>

						<div class="formhelp">
							<fmt:message key="login.help"></fmt:message>
						</div>

						<table>
							<tr>
								<td colspan="2" class="formhelp"><wiki:Messages div="error"
										topic="login"
										prefix='<%=LocaleSupport.getLocalizedMessage(pageContext,"login.errorprefix")%>' />
								</td>
							</tr>
							<tr>
								<td><label for="j_username"><fmt:message key="login.login" /></label></td>
								<td><input type="text" size="24"
									value="<wiki:Variable var='uid'	default='' />"
									name="j_username" id="j_username" /></td>
							</tr>
							<tr>
								<td><label for="j_password"><fmt:message
											key="login.password" /></label></td>
								<td><input type="password" size="24" name="j_password"
									id="j_password" /></td>
							</tr>
							<% if( supportsCookieAuthentication ) { %>
							<tr>
								<td><label for="j_remember"><fmt:message
											key="login.remember" /></label></td>
								<td><input type="checkbox" name="j_remember"
									id="j_remember" /></td>
							</tr>
							<% } %>
							<tr>
								<td>&nbsp;</td>
								<td><input type="hidden" 
									name="redirect"
									value="<wiki:Variable var='redirect' default='' />" /> 
									<input type="submit" name="submitlogin"
										value="<fmt:message key='login.submit.login'/>" />
								</td>
							</tr>
						</table>

						<div class="formhelp">
							<fmt:message key="login.lostpw" />
							
							<a href="#" onclick="$('#lostpasswordTab').click();return false;"
								title="<fmt:message key='login.lostpw.title'/>"> 
								<fmt:message key="login.lostpw.getnew" />
							</a>
						</div>

						<div class="formhelp">
							<fmt:message key="login.nopassword" />
							<a href="#" onclick="$('#profileTab').click();return false;"
								title="<fmt:message key='login.registernow.title'/>"> 
								<fmt:message key="login.registernow">
									<fmt:param>
										<wiki:Variable var="applicationname" />
									</fmt:param>
								</fmt:message>
							</a>
						</div>
					</div>
				</form>
			</div>

			<div id="lostpassword" class="tab-pane">

				<div class="center">
					<form action="<wiki:Link jsp='LostPassword.jsp' format='url'><wiki:Param name='tab' value='lostpassword'/></wiki:Link>"
						id="lostpw" class="wikiform"
						onsubmit="return Wiki.submitOnce(this);" method="post"
						accept-charset="<wiki:ContentEncoding />">

						<h3>
							<fmt:message key="login.lostpw.heading" />
						</h3>

						<c:choose>
							<c:when test="${passwordreset == 'done' }">
								<wiki:Messages div="information" topic="resetpw" prefix="" />
								<p>
									<fmt:message key="login.lostpw.reset.login">
										<fmt:param>
											<a href="<wiki:Link jsp='Login.jsp' />"><fmt:message key="login.lostpw.reset.clickhere" /></a>
										</fmt:param>
									</fmt:message>
								</p>
							</c:when>
							<c:otherwise>

								<div class="formhelp">
									<fmt:message key="login.lostpw.help"></fmt:message>
								</div>

								<table>
									<c:if test="${param.tab eq 'lostpassword'}">
										<tr>
											<td colspan="2" class="formhelp">
												<wiki:Messages
													div="error" topic="resetpw"
													prefix='<%=LocaleSupport.getLocalizedMessage(pageContext,"login.errorprefix")%>' />
												<wiki:Messages div="information" topic="resetpwok" />
											</td>
										</tr>
									</c:if>
									<tr>
										<td><label for="name"><fmt:message	key="login.lostpw.name" /></label></td>
										<td><input type="text" size="24" name="name" id="name" /></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td><input type="hidden" name="action"
											value="resetPassword" /> <input type="submit" name="Submit"
											value="<fmt:message key='login.lostpw.submit'/>" /></td>
									</tr>
								</table>

								<div class="formhelp">
									<fmt:message key="login.invite" />
									<a href="#"	onclick="$('#logincontentTab').click();return false;"
										title="<fmt:message key='login.title'/>"> <fmt:message
											key="login.heading.login">
											<fmt:param>
												<wiki:Variable var="applicationname" />
											</fmt:param>
										</fmt:message>
									</a>
								</div>
								<div class="formhelp">
									<fmt:message key="login.nopassword" />
									<%--<a href="UserPreferences.jsp?tab=profile">--%>
									<a href="#" onclick="$('#profileTab').click();return false;"
										title="<fmt:message key='login.registernow.title'/>"> <fmt:message
											key="login.registernow">
											<fmt:param>
												<wiki:Variable var="applicationname" />
											</fmt:param>
										</fmt:message>
									</a>
								</div>
							</c:otherwise>
						</c:choose>
					</form>
				</div>
			</div>
		</wiki:UserCheck>

		<wiki:Permission permission='editProfile'>
			<div id="profile" class="tab-pane">
				<wiki:Include page='ProfileTab.jsp' />
			</div>
		</wiki:Permission>

		<div id="loginhelp" class="tab-pane">
			<wiki:InsertPage page="LoginHelp" />
			<wiki:NoSuchPage page="LoginHelp">
				<div class="error">
					<fmt:message key="login.loginhelpmissing">
						<fmt:param>
							<wiki:EditLink page="LoginHelp">LoginHelp</wiki:EditLink>
						</fmt:param>
					</fmt:message>
				</div>
			</wiki:NoSuchPage>
		</div>
			</div>
	</c:otherwise>
</c:choose>