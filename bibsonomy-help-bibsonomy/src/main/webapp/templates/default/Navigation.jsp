<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="wiki_lang"><wiki:Variable var='jspwiki.Language' /></c:set>
<c:set var="wiki_project_name"><wiki:Variable var='jspwiki.projname' /></c:set>


<fmt:setLocale value="${wiki_lang}"/>
<fmt:setBundle basename="messages"/>
<%
  WikiContext c = WikiContext.findContext(pageContext);
  String frontpage = c.getEngine().getFrontPage(); 
%>

<nav class="navbar navbar-bibsonomy" role="banner">
	<div class="container navi noborder">
		<div class="collapse navbar-collapse" id="bs-navbar-collapse">
			
			<ul class="nav navbar-nav">
				<li class=""><a href="/"><fmt:message key="home"/></a></li>
				<li class=""><a href="/myBibSonomy"><fmt:message key="my"/><c:out value="${wiki_project_name}"/></a></li
			</ul>
			
			<%--
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="fa fa-user"><!-- KEEP ME --></span>
						<b class="caret"></b>
					</a>
				</li>
			</ul>
			 --%>
			
		</div>
	</div>
</nav>