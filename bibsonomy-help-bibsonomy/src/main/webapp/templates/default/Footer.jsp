<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="wiki_lang"><wiki:Variable var='jspwiki.Language' /></c:set>
<c:set var="helpLangUrl"><wiki:Variable var='jspwiki.baseURL' /></c:set>

<fmt:setLocale value="${wiki_lang}"/>
<fmt:setBundle basename="messages"/>

<script type="text/javascript">
	var pkBaseURL = (("https:" == document.location.protocol) ? "https://stats.bibsonomy.org/" : "http://stats.bibsonomy.org/");
	document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
	try {
		var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 1);
		piwikTracker.trackPageView();
		piwikTracker.enableLinkTracking();
	} catch( err ) {}
</script><noscript><p><img alt="" style="border:0" src="http://stats.bibsonomy.org/piwik.php?idsite=1"/></p></noscript>

<div class="container footer">
	<div id="footer-more">
		<nav class="row">
			<dl class="col-md-3 col-sm-6">
				<dt><fmt:message key="footer.what"><fmt:param><wiki:Variable var='jspwiki.projname' /></fmt:param></fmt:message></dt>
				
				<dd><a href="/gettingStarted"><fmt:message key="footer.quickstart"/></a></dd>
				<dd><a href="/buttons"><fmt:message key="footer.buttons"/></a></dd>
				<dd><a href="${helpLangUrl}Main"><fmt:message key="help"/></a></dd>
				
				
				<dt><fmt:message key="footer.developer"/></dt>
			
				<dd><span class="fa fa-bitbucket"><!--  --></span>&nbsp;<a href="http://bibsonomy.bitbucket.org/"><fmt:message key="footer.overview"/></a></dd>
				<dd><span class="fa fa-puzzle-piece"><!--  --></span>&nbsp;<a href="http://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API"><fmt:message key="settings.apidoc"/></a></dd>
			</dl>
			
			<dl class="col-md-3 col-sm-6">
				<dt><fmt:message key="footer.privacyContact"/></dt>
				
				<fmt:message var="contactLink" key="footer.contact.link"/>
				<fmt:message var="termsOfUseLink" key="system.termsOfUse.link"/>
				<fmt:message var="cookiesLink" key="footer.cookies.link"/>
				
				<dd><a href="${helpLangUrl}${contactLink}"><fmt:message key="navi.about"/></a></dd>
				<dd><a href="${helpLangUrl}${termsOfUseLink}"><fmt:message key="system.termsOfUse"/></a></dd>
				<dd><a href="${helpLangUrl}${cookiesLink}"><fmt:message key="footer.cookies"/></a></dd>
				<dd><a href="http://bitbucket.org/bibsonomy/bibsonomy/issues"><fmt:message key="issues"/></a></dd>
				<%--TODO FIX <dd><a href="https://bitbucket.org/bibsonomy/bibsonomy/wiki"><c:out value="${properties['project.name']}" /> Wiki</a></dd>--%>
			</dl>
	
			<dl class="col-md-3 col-sm-6">
				<dt><fmt:message key="footer.integration"/></dt>
			
				<dd><a href="http://academic-puma.de/"><fmt:message key="footer.academicPuma"/></a></dd>
				<dd><a href="http://typo3.org/extensions/repository/view/ext_bibsonomy_csl"><fmt:message key="typoThree.plugin"/></a></dd>
				<dd><a href="http://wordpress.org/plugins/bibsonomy-csl/"><fmt:message key="wordpress.plugin"/></a></dd>
				<dd><a href="http://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-rest-client/"><fmt:message key="restClient.java"/></a></dd>
				<dd><a href="/scraperinfo"><fmt:message key="footer.scraperInfo" /></a></dd>
				<dd><a href="${helpLangUrl}/Integration"><fmt:message key="more"/></a></dd>
			</dl>
				
			<dl class="col-md-3 col-sm-6">
				<dt>
					<fmt:message key="footer.about" />&nbsp;<wiki:Variable var='jspwiki.projname' />
				</dt>
				
				<dd><a href="${helpLangUrl}/Team"><fmt:message key="footer.team"/></a></dd>
				<%-- TODO FIX:<dd><a href="${properties['project.blog']}"><fmt:message key="blog"/></a></dd> --%>
				<dd><a href="https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss"><fmt:message key="footer.mailingList"/></a></dd>
				
				<dt>Social Media</dt>
				<dd><span class="fa fa-twitter"><!--  --></span>&nbsp;<a href="http://twitter.com/bibsonomyCrew"><fmt:message key="footer.twitter"/></a></dd>
				<dd><span class="fa fa-google"><!--  --></span>&nbsp;<a href="https://plus.google.com/communities/102073418538419220890">Google+ Community</a></dd>
			</dl>
	
		</nav>
	
		<div class="legal-notices">
			<hr />
			<p>
				<fmt:message key="system.offeredBy">
					<fmt:param><wiki:Variable var='jspwiki.projname' /></fmt:param>
				</fmt:message>
			</p>
		</div>
	</div>
</div>